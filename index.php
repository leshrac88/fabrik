<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="public/jquery-2.0.0.js"></script>

    <title>test</title>
    <script type="text/javascript">
        function runJS(src)
        {
            var tag = document.createElement("SCRIPT");
            tag.src = src;
            document.getElementsByTagName('HEAD')[0].appendChild(tag);
        }
    </script>
    <style type="text/css">
        .button-td {
            text-align: center;
            vertical-align: middle;
        }

        .main-td {
            width: 75%
        }

        .code-block {
            background-color: #FAF3D2;
            font-style: italic;
            border-style: solid;
            border-width: 1px;
        }
        html {
            background-color: #F8F7F2;
        }
    </style>
</head>
<body>
    <table border="2" cellpadding="5" align="center" width="60%">
        <tr>
            <td class="main-td">Есть массив целых чисел идущих по порядку [1,2,5,7, ... 158 .... 10000000].
                Напишите оптимальную функцию которая определит есть ли число <b>42</b> в этом массиве </td>
            <td class="button-td"><form action="tasks/1/main.php"><button type="submit">Задание 1</button></td></form>
        </tr>
        <tr>
            <td class="main-td">
                У нас есть массив подготовленных к записи сообщений который выглядит так :
                <br>
                <br>
                <pre class="code-block">
array(
    array(
        'firm_id' => N,
        'subject' => S,
        'body'    => S,
        'from'    => S,
        'to'      => S
    ),
    .....
    array(
        'firm_id' => N,
        'subject' => S,
        'body'    => S,
        'from'    => S,
        'to'      => S
    )
)                </pre>
                <br>
                Сообщения пишутся в N количество баз данных с одинаковой структурой, сервер на который необходимо записать данные определяется в зависимости от firm_id, напишите оптимальный, по вашему мнению, алгоритм обработки и записи этих данных
            </td>
            <td class="button-td"><form><button disabled type="submit">Задание 2</button></td></form>
        </tr>
        <tr>
            <td class="main-td">
                Есть таблица books со структурой:
                <br>
                <pre class="code-block">book_id, title, description, author_id, status, date (timestamp)</pre>
                Таблица authors:
                <pre class="code-block">author_id, name</pre>
                Предположим что у нас в базе храниться 1 000 000 книг и нам необходимо сделать вывод списка книг с авторами и постраничной навигацией, книги со статусом (status) равным двум и отсортированным по дате (date)
                Напишите как бы вы это сделали и какие индексы (если нужно) вы бы добавили к таблицам и почему ?
            </td>
            <td class="button-td"><form action="tasks/3/main.php"><button type="submit">Задание 3</button></td></form>
        </tr>
        <tr>
            <td class="main-td">
                Опишите класс Book (модель) при учете того, что у нас:
                <ol type="a">
                    <li>Active Record</li>
                    <li>Data Mapper</li>
                </ol>
                т.е. нам нужно 2-а варианта класса. Что еще необходимо помимо класса Book?
            </td>
            <td class="button-td"><form action="tasks/4/main.php"><button type="submit">Задание 4</button></td></form>
        </tr>
        <tr>
            <td class="main-td">Есть задача писать логи в СУБД MySql, какое хранилище выбрать, почему, какие настройки ?</td>
            <td class="button-td"><form><button type="button" onclick='alert("5");'value="5">Задание 5</button></td></form>
        </tr>
        <tr>
            <td class="main-td">Какой будет файл конфигурации (часть server) у Nginx если : Запросы к домену test.ru должны попадать на один 4 физических серверов (A, B, C, D) для пользователя с привязкой по IP, т.е. пользователь с определенным ip будет всегда попадать на определенную машину, вместе с тем для раздела test.ru/news/ предусмотрено только 2 сервера (A, C) </td>
            <td class="button-td"><form action="tasks/6/main.php"><button type="submit">Задание 6</button></td></form>
        </tr>
    </table>
</body>
</html>