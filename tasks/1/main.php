<?php
$time_start = microtime(true);
echo "Start...<br>";

require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'numberExists.php';

define("NUM_ITERATION", 10, true);

$total_standard = 0;
$total_linear = 0;
$total_binary = 0;
$total_in = 0;

for ($i = 0; $i < NUM_ITERATION; $i++)
{
    $v = new NumberExists();

    $result = "";
    $time_standard_start = microtime(true);
    $result = $v->standardSearch($v->arr);
    $time_standard_end = microtime(true);
    $time_standard = $time_standard_end - $time_standard_start;
    $total_standard += $time_standard;

    $result = "";
    $time_linear_start = microtime(true);
    $result = $v->linearSearch($v->arr);
    $time_linear_end = microtime(true);
    $time_linear = $time_linear_end - $time_linear_start;
    $total_linear += $time_linear;

    $result = "";
    $time_binary_start = microtime(true);
    $result = $v->binarySearch($v->arr);
    $time_binary_end = microtime(true);
    $time_binary = $time_binary_end - $time_binary_start;
    $total_binary += $time_binary;

    $result = "";
    $time_in_start = microtime(true);
    $result = $v->standardIn($v->arr);
    $time_in_end = microtime(true);
    $time_in = $time_in_end - $time_in_start;
    $total_in += $time_in;
}

$time = $total_standard / NUM_ITERATION;
$time = sprintf('%.6f', $time);
echo "Standart search(<i>array_search()</i>) run $time seconds<br>";

$time = $total_linear / NUM_ITERATION;
$time = sprintf('%.6f', $time);
echo "Linear search run $time seconds<br>";

$time = $total_binary / NUM_ITERATION;
$time = sprintf('%.6f', $time);
echo "Binary search run $time seconds<br>";

$time = $total_in / NUM_ITERATION;
$time = sprintf('%.6f', $time);
echo "Standart search(<i>in_array()</i>) run $time seconds<br>";

$time_end = microtime(true);
$time_run = $time_end - $time_start;
$time_run = sprintf('%.6f', $time_run);
echo "End...<br>Script will be run $time_run seconds<br>";