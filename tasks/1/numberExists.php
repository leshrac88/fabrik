<?php

// min value of element in array
define("ARR_MIN", 1, true);
// max value of element in array
define("ARR_MAX", 10000000, true);
// search value
define("NUM_SEARCH", 42, true);

// проверка массива на кол-во значений, в двоичном - четные-нечетные обратить внимание
// если в массиве несколько искомых элементов...
// проверка длины first+last < 2^31
// NUM_SEARCH is_int()
class NumberExists
{
    public $numArray;
    public $arr;

    /**
     * initialization and filling array
     *
     * $numArray - length of array
     * $arr - array of elements
     * $elNum - number of current element
     * $elVal - value of current element
     * @return array $arr
     */
    public function NumberExists()
    {
        $numArray = mt_rand(ARR_MIN, ARR_MAX);
        $this->numArray = $numArray;

        $arr = array();
        for ($elNum = 0; $elNum < $numArray; $elNum++)
        {
            $elVal = mt_rand(ARR_MIN, ARR_MAX);
//            array_push($arr, $elVal);
            $arr[$elNum] = $elVal;
//            $arr = array_fill($elNum, 1, $elVal);
        }

        // use standard function for sort array
        sort($arr);
        $this->arr = $arr;
    }

    /*
     * Binary Search NUM_SEARCH in $arr
     * @return $elNum | false
     */
    public function binarySearch($arr)
    {
        $first = 0;
        $last = $this->numArray - 1;

        while (true)
        {
            $count = $last - $first;

            // if number of elements in array is odd
            // ($count % 2 != 0) ? $count++;
            if ($count > 2)
            {
                if ($count % 2 != 0)
                {
                    $count++;
                }

                $mid = (int) (($last - $first) / 2) + $first;
            }
            elseif ($count >= 0)
            {
                $mid = $first;
            }
            else
            {
                return false;
            }

            if ($arr[$mid] == NUM_SEARCH)
            {
                while (($mid != 0) && ($arr[$mid - 1] == NUM_SEARCH))
                    $mid--;
                return $mid;
            }
            elseif ($arr[$mid] > NUM_SEARCH)
            {
                $last = $mid - 1;
            }
            else{
                $first = $mid + 1;
            }
        }
    }

    /*
     * Linear Search NUM_SEARCH in $arr
     * @return $elNum | false
     */
    public function linearSearch($arr)
    {
        for ($elNum = 0; $elNum < $this->numArray; $elNum++)
        {
            if ($arr[$elNum] == NUM_SEARCH)
                return $elNum;
        }

        return false;
    }

    /*
     * Use standard function array_search for search element in array
     */
    public function standardSearch($arr)
    {
        return array_search(NUM_SEARCH, $arr);
    }

    /*
     * Use standard function in_array for search element in array
     */
    public function standardIn($arr)
    {
        return in_array(NUM_SEARCH, $arr);
    }
}