<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--<script type="text/javascript" src="public/jquery-2.0.0.js"></script>-->

    <title>test</title>
    <style type="text/css">
        .button-td {
    text-align: center;
            vertical-align: middle;
        }

        .main-td {
    width: 75%
        }

        .code-block {
    background-color: #FAF3D2;
            font-style: italic;
            border-style: solid;
            border-width: 1px;
        }
        html {
    background-color: #F8F7F2;
        }
    </style>
</head>
<body>
<?php

$authors = "sql".DIRECTORY_SEPARATOR."tAuthors";
$books = "sql".DIRECTORY_SEPARATOR."tBooks";
$fillAuthors = "sql".DIRECTORY_SEPARATOR."pFillAuthors";
$fillBooks = "sql".DIRECTORY_SEPARATOR."pFillBooks";
$getBooks = "sql".DIRECTORY_SEPARATOR."pGetBooks";

require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'functions.php';
?>
<?php
    if (checkFile($authors) && checkFile($books) && checkFile($fillAuthors) && checkFile($fillBooks) && checkFile($getBooks))
    {
        echo "Создаем таблицу <i>tAuthors</i>:<br>";
        echo '<pre class="code-block">';
        echo file_get_contents($authors);
        echo "</pre>";

        echo "Создаем таблицу <i>tBooks</i>:<br>";
        echo '<pre class="code-block">';
        echo file_get_contents($books);
        echo "</pre>";

        echo "Заполняем таблицу <i>tAuthors</i>:<br>";
        echo '<pre class="code-block">';
        echo file_get_contents($fillAuthors);
        echo "</pre>";

        echo "Заполняем таблицу <i>tBooks</i>:<br>";
        echo '<pre class="code-block">';
        echo file_get_contents($fillBooks);
        echo "</pre>";

        echo "Хранимка для вывода книг со статусом равным <b>2</b> с авторами отсортированных по дате постранично (по 10 штук):<br>";
        echo '<pre class="code-block">';
        echo file_get_contents($getBooks);
        echo "</pre>";
    }
    ?>

</body>
</html>
