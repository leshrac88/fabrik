<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--<script type="text/javascript" src="public/jquery-2.0.0.js"></script>-->

    <title>test</title>
    <style type="text/css">
        .button-td {
            text-align: center;
            vertical-align: middle;
        }

        .main-td {
            width: 75%
        }

        .code-block {
            background-color: #FAF3D2;
            font-style: italic;
            border-style: solid;
            border-width: 1px;
        }
        html {
            background-color: #F8F7F2;
        }
    </style>
</head>
<body>
a. Active Record (Yii):<br>
<pre class="code-block">
class Book extends \BaseActiveRecord
{
    public function tableName()
    {
        return "{{book}}";
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return [
            ["title, author_id, date", "required"],
            [],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t("book.title"),
            'description' => Yii::t("book.description"),
        ];
    }

    public function relations()
    {
        return [
            'author' => [self::HAS_MANY, 'path/to/models/author', ["author_id" => "id"]],
        ];
    }
}
</pre>
<br>
<br>
b. Data Mapper (Zend):<br>
<pre class="code-block">
class Model_DbTable_Books extends Zend_Db_Table_Abstract
{
    protected $_name = 'books';

    protected $_rowClass = 'Model_DbRow_Book';
}


class Model_DbRow_Book extends Model_DbRow_Abstract
{
    public function __toString()
    {
        return (string) $this->name;
    }

    public function toArray()
    {
        $data = parent::toArray();
        $data['id'] = (int) $data['id'];
        $data['title'] = (string) $data['title'];
        $data['description'] =(string) $data['description'];
        $data['author_id'] = (int) $data['author_id'];
        $data['status'] =(bool) $data['status'];
        $data['date'] =(string) $data['date'];

    }

    public function getAuthor()
    {
        if (!empty($this->author_id))
        {
            $author = $this->findParentRow('Model_DbTable_Authors');
            return !empty($author) ? $author->name : 'author not found';
        }
    return '';
    }
}
</pre>

</body>
</html>