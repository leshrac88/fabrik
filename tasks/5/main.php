<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--<script type="text/javascript" src="public/jquery-2.0.0.js"></script>-->

    <title>test</title>
    <style type="text/css">
        .button-td {
            text-align: center;
            vertical-align: middle;
        }

        .main-td {
            width: 75%
        }

        .code-block {
            background-color: #FAF3D2;
            font-style: italic;
            border-style: solid;
            border-width: 1px;
        }
        html {
            background-color: #F8F7F2;
        }
    </style>
</head>
<body>
<!--Тип хранилища MyISAM, так как при работе с БД будут преобладать операции <i>select</i> и <i>insert (но некоторые пишут, что инсерты лучше в иннодб)</i>.
Транзакции и внешние ключи (на первый взгляд) не нужны, блокировка на уровне таблицы не критична. Наличие concurrent_insert.
Возможно, стоит увеличить back_log, delayed_insert_timeout, delayed_insert_limit...-->
Тип хранилища InnoDB. Быстрее отрабатывают <i>insert</i>, построчная блокировка. InnoDB под нагрузкой (или на больших таблицах) быстрее. Надежнее, чем MyISAM, не падают индексы,
сама чинит поврежденные таблицы. Наличие внешних ключей, транзакций. Полнотекстовый поиск не нужен (на перый взгляд).
</body>
</html>