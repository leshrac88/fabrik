<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--<script type="text/javascript" src="public/jquery-2.0.0.js"></script>-->

    <title>test</title>
    <style type="text/css">
        .button-td {
            text-align: center;
            vertical-align: middle;
        }

        .main-td {
            width: 75%
        }

        .code-block {
            background-color: #FAF3D2;
            font-style: italic;
            border-style: solid;
            border-width: 1px;
        }
        html {
            background-color: #F8F7F2;
        }
    </style>
</head>
<body>
При наличии нескольких физических серверов:<br>
<pre class="code-block">
    location /news {
        set $val 1;
    }
    server {
        if ($val = 1)
        {
            listen A:80;
            listen C:80;
        }
        if ($val != 1)
        {
            listen A:80;
            listen B:80;
            listen C:80;
            listen D:80;
        }
    }
</pre>
</body>
</html>